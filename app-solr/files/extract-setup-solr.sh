#!/bin/bash

MODULE_DIR=/tmp/hybris/bin/modules/search-and-navigation/solrserver
RESOURCES_DIR=$MODULE_DIR/resources/solr

die() {
  echo $1
  exit $2
}
SCRIPT_PATH=$(cd $(dirname $0) && pwd)
[ $(id -u) -eq 0 ] || die "script must be run as root user to set up permissions" 2
cd /tmp/hybris || die "can't change dir to /tmp/hybris" 3

umask 002
cd /tmp/hybris

HYBRIS_SOLR_DIR=$RESOURCES_DIR
test -d $HYBRIS_SOLR_DIR || die "no hybris solr dir $HYBRIS_SOLR_DIR found" 10
echo Found solr dir $HYBRIS_SOLR_DIR

SERVER_DIR=$(ls -d $HYBRIS_SOLR_DIR/$1/server 2>/dev/null)
CHANGES_FILE=$SERVER_DIR/CHANGES.txt
test -f $CHANGES_FILE || die "no $CHANGES_FILE found" 11
SOLR_VERSION=$(grep ==== $CHANGES_FILE |head -n 1|tr -d ' =\r')
echo $SOLR_VERSION | grep -q ^[6789] || die "can't extract Solr version from $CHANGES_FILE" 12
echo Found $CHANGES_FILE with solr version $SOLR_VERSION

SOLR=/opt/solr

/bin/rm -rf $SOLR/$SOLR_VERSION $SOLR/current $SOLR/latest
rm -rf $SOLR/$SOLR_VERSION
mkdir -p $SOLR/$SOLR_VERSION
mkdir -p $SOLR/java
cp -af $SERVER_DIR/. $SOLR/$SOLR_VERSION/
ln -s $SOLR_VERSION $SOLR/current
ln -s current $SOLR/latest
echo Installed in $SOLR/$SOLR_VERSION

sed -i '/running=.lsof/s,running.*,running=`lsof -PniTCP:$SOLR_PORT -sTCP:LISTEN 2>/dev/null || lsof -i TCP:$SOLR_PORT -T s|grep "TCP.*:$SOLR_PORT .*LISTEN"`,' $SOLR/$SOLR_VERSION/bin/solr

mkdir -p $SOLR/$SOLR_VERSION/server/solr/configsets/backoffice/conf/
for f in lang stopwords.txt synonyms.txt
do
  cp -af $SOLR/$SOLR_VERSION/server/solr/configsets/default/conf/$f $SOLR/$SOLR_VERSION/server/solr/configsets/backoffice/conf/$f
done

ln -s /opt/java/sapmachine-jdk $SOLR/java/default 2>/dev/null

scripts/system-setup/setup-passwd-and-group.sh &>/dev/null
mkdir -p $SOLR/$SOLR_VERSION/server/logs $SOLR/$SOLR_VERSION/supervise
chown solr:solr $SOLR/.
chown -h solr:solr $SOLR/java $SOLR/current $SOLR/latest
chown -R solr:solr $SOLR/$SOLR_VERSION/.
chmod o-rwx "$SOLR/$SOLR_VERSION"*
chmod -R g-w $SOLR/$SOLR_VERSION/.
chmod g+w $SOLR/$SOLR_VERSION/bin $SOLR/$SOLR_VERSION/server/logs
chmod g+w $SOLR/$SOLR_VERSION/server/solr/cores 2>/dev/null || chmod g+w $SOLR/$SOLR_VERSION/server/solr
chmod 750 $SOLR/$SOLR_VERSION/bin/solr
echo "***DONE***"
exit 0
